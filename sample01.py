# coding: utf-8

import yaml
from random import *
from pprint import pprint



class Stock:
  def __init__(self, name):
    self.name = name

class Order:
  def __init__(self, stock, side, price, amount):
    self.stock = stock
    self.side = side
    self.price = price
    self.amount = amount

class Position:
  def __init__(self, stock, avg_price, amount):
    self.stock = stock
    self.avg_price = avg_price
    self.amount = amount

  def update(self, order):
    if order.side == 'buy':
      self.price = ((self.avg_price * self.amount) + (order.price * order.amount)) / (self.amount + order.amount)
      self.amount += order.amount
    elif order.side == 'sell':
      self.amount -= order.amount

class Portfolio:
  def __init__(self, pos_map={}):
    self.pos_map = pos_map

  def get(self, name):
    return self.pos_map.get(name)

  def insert(self, pos):
    pos_map[pos.stock.name] = pos

  def update(self, order):
    pos = get(order.stock.name)
    pos.update(order)
    self.pos_map[pos.stock.name] = pos

  def delete(self, pos):
    self.pos_map.remove(pos.stock.name)

class SecuritiesFirm:
  def __init__(self, balance=0, portfolio=None):
    self.balance = balance
    self.portfolio = portfolio if portfolio != None else Portfolio()

  def trade(self, order):
    had_pos = self.portfolio.get(order.stock.name)
    trade_price = order.price * order.amount
    
    if order.side == 'buy':
      if self.balance < trade_price:
        return False
      if had_pos == None:
        self.portfolio.insert(Position(order.stock, order.price, order.amount))
      else:
        self.portfolio.update(order)
      self.balance -= trade_price
    
    elif order.side == 'sell':
      if had_pos == None:
        return False
      elif had_pos.amount < amount:
        return False
      self.portfolio.update(order)
      self.balance += trade_price

    return True

  def getBalance(self):
    return self.balance

  def getAmount(self, name):
    pos = self.portfolio.get(name)
    return pos.amount if pos != None else 0


data = []
with open('pricedata/data01.yaml', 'r') as file:
  data = yaml.load(file)

act_type = ['buy', 'sell', 'stay']

epsilon = 0.3
# 割引率
gamma = 0.9
# 学習係数
alpha = 0.1

if __name__ == '__main__':
  fisco = Stock('fisco')
  rakuten = SecuritiesFirm(10)

  Q = [{type: uniform(0, 99) for type in act_type} for i in xrange(100)]

  theta = 1e-5
  delta = 0.0

  while delta < theta:
    delta = 0.0

    # 時間経過
    for t in xrange(len(data)):
      while True:
        q = Q[t]
        side = max(q, key=lambda k: q[k])
        if random() <= epsilon:
          types = act_type[::]
          types.remove(side)
          side = types[randint(0, len(types) - 1)]

        order = Order(fisco, side, data[t], 1)
        if rakuten.trade(order):
          print order.stock.name, order.side, order.price, order.amount
          print rakuten.getAmount('fisco')

          print 'test'
          break
